Overview of Invoice System

The following is how I read (and would program) that assignment. 


+ Abstract Class: `BillForm`
	+ Class (static) variables:
		+ static int `chequeId` - Holds the last issued cheque number
	+ Instance variables (these variables are common for both, `InvoicePayment` and `RecurringBillPayment`):
		+ int `id` - holds a unique ID for each bill/cheque
		+ String `name` - The name of the person or company being paid
		+ double `amount` - the amount to be paid
		+ Date `date` - date of payment
	+ Class (static) methods:
		+ static int getUniqueChequeId() - increments the number stored in `chequeId` and returns this number
	+ Abstract methods (to be implemented in subclasses):
		+ void getFormInfo() - Data entry form for the individual subclasses

----

+ Interface `ChequeGenerator`
	+ Interface method:
		+ void printCheque() - must be overridden by the implementing classes to print out a cheque

----

+ Concrete Class: `InvoicePayment` extends `BillForm` implements `ChequeGenerator`:
	+ Instance variables:
		+ int invoiceNumber - the number of the invoice being paid
	+ Instance methods:
		+ `InvoicePayment()` - constructor - call `BillForm.getUniqueChequeId()` to get a new Cheque ID and store it in the `id` instance variable
		+ @Override void `getFormInfo()` - form to fill the data - overridden from Abstract class
		+ @Override printCheque() - implementation of the interface method

----

+ Concrete Class: `RecurringBillPayment` extends `BillForm` implements `ChequeGenerator`:
	+ Instance variables:
		+ Date `dateStart`
		+ Date `DateEnd`
	+ Instance methods:
		+ `RecurringBillPayment()` - constructor - call `BillForm.getUniqueChequeId()` to get a new Cheque ID and store it in the `id` instance variable
		+ @Override void `getFormInfo()` - form to fill the data - overridden from Abstract class
		+ @Override printCheque() - implementation of the interface method

----

+ Concrete Class: `PayrollPayment` implements `ChequeGenerator`
	+ Instance variables:
		+ int `id` - unique cheque ID
		+ String `name` - Name of the employee
		+ double `amount` - amount to pay
		+ Date `date` - pay date
	+ Instance methods:
		+ `PayrollPayment(String name, double amount)` - Constructor, uses `BillForm.getUniqueChequeId()` to get the `id` and also `new Date()` to get the date paid
		+ @Override printCheque() - implementation of the interface method

----

+ Main Program 
	+ Here, you will need an array of `ChequeGenerator` - the common interface to all three concrete classes so that you can store all different objects there
	+ The rest should be fairly clear from the instructions.


----