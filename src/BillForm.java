import java.util.Date;
import java.util.Scanner;


public abstract class BillForm {
	// Class variable
	private static int chequeId = 0;
	
	// Instance variables
	protected int id;
	protected String name;
	protected double amount;
	protected Date date;
	
	// static method accessible everywhere
	public static int getUniqueChequeId() {
		return chequeId++;
	}
	
	// Abstract methods
	public abstract void getFormInfo(Scanner kb);
}
