import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class InvoiceMain {

	public static void main(String[] args) {
		List<ChequeGenerator> items = new ArrayList<>();
		Scanner kb = new Scanner(System.in);
		
		InvoicePayment bill1 = new InvoicePayment();
		RecurringBillPayment bill2 = new RecurringBillPayment();
		
		bill1.getFormInfo(kb);
		bill2.getFormInfo(kb);
		
		System.out.println("\nPlease enter the data for the Payroll Payment:\n");
		System.out.print("Please enter the recipient name: ");
		String name = kb.nextLine();
		System.out.print("Please enter the amount to be paid: ");
		double amount = Double.parseDouble(kb.nextLine());
		
		PayrollPayment bill3 = new PayrollPayment(name, amount);
		
		items.add(bill1);
		items.add(bill2);
		items.add(bill3);
		
		for(ChequeGenerator bill : items) {
			bill.printCheque();
		}

	}

}
