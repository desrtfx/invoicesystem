import java.util.Date;
import java.util.Scanner;


public class InvoicePayment extends BillForm implements ChequeGenerator {

	private int invoiceNumber;
	
	public InvoicePayment() {
		id = BillForm.getUniqueChequeId();
		date = new Date();
	}
	
	
	@Override
	public void printCheque() {
		System.out.println("********** Invoice Cheque **********\n");
		System.out.println("Cheque Id: " + id);
		System.out.println("Issued in favor of: " + name);
		System.out.println("Invoice number: " + invoiceNumber);
		System.out.println("Date paid: " + date.toString());
		System.out.println("Amount: " + amount);
		System.out.println("************************************\n\n");

	}

	@Override
	public void getFormInfo(Scanner kb) {
		System.out.println("\nPlease enter the data for the Invoice Cheque:\n");
		System.out.print("Enter the name of the recipient person/company: ");
		this.name = kb.nextLine();
		System.out.print("Enter the amount to be paid: ");
		this.amount = Double.parseDouble(kb.nextLine());
		System.out.print("Enter the invoice number to be paid: ");
		this.invoiceNumber = Integer.parseInt(kb.nextLine());
	}

}
