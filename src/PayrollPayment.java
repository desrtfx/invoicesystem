import java.util.Date;


public class PayrollPayment implements ChequeGenerator {

	private int id;
	private String name;
	private double amount;
	private Date date;	
	
	public PayrollPayment(String name, double amount) {
		id = BillForm.getUniqueChequeId();
		date = new Date();
		this.name = name;
		this.amount = amount;
	}
	
	
	@Override
	public void printCheque() {
		System.out.println("********** Payroll Cheque **********\n");
		System.out.println("Cheque Id: " + id);
		System.out.println("Issued in favor of: " + name);
		System.out.println("Date paid: " + date.toString());
		System.out.println("Amount: " + amount);
		System.out.println("************************************\n\n");

	}

}
