import java.util.Date;
import java.util.Scanner;


public class RecurringBillPayment extends BillForm implements ChequeGenerator {

	private String startDate;
	private String endDate;

	public RecurringBillPayment() {
		id = BillForm.getUniqueChequeId();
		date = new Date();
	}
	
	
	@Override
	public void printCheque() {
		System.out.println("********** Recurring Bill Cheque **********\n");
		System.out.println("Cheque Id: " + id);
		System.out.println("Issued in favor of: " + name);
		System.out.println("For the period from: " + startDate + " to: " + endDate);
		System.out.println("Date paid: " + date.toString());
		System.out.println("Amount: " + amount);
		System.out.println("*******************************************\n\n");
	}

	@Override
	public void getFormInfo(Scanner kb) {
		System.out.println("\nPlease enter the data for the Recurring Bill Payment:\n");
		System.out.print("Enter the name of the recipient person/company: ");
		this.name = kb.nextLine();
		System.out.print("Enter the amount to be paid: ");
		this.amount = Double.parseDouble(kb.nextLine());
		System.out.print("Enter the start date of the period: ");
		this.startDate = kb.nextLine();
		System.out.print("Enter the end date of the period: ");
		this.endDate = kb.nextLine();
		
	}

}
